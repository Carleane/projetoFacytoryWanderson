/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod.fabrica;

import factorymetthod.cliente.Cliente;
import factorymetthod.cliente.ClienteComum;
import factorymetthod.cliente.ClientePreferencial;

/**
 *
 * @author Carleane sousa
 */
public class ClienteFactory {
    
    public Cliente criarCliente(String nome , int idade){
        Cliente c;
        if (idade>=65){
            c = new ClientePreferencial(nome, idade);
        }else{
            c = new ClienteComum(nome, idade);
        }
        return c;
    }
    }
    

