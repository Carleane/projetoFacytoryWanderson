
package factorymethod.fabrica;

import factorymethod.carro.Brisma;
import factorymethod.carro.Carro;
import factorymethod.carro.Onix;
import factorymethod.interfaces.FabricaCarro;
import java.math.BigDecimal;

/**
 *
 * @author Carleane sousa
 */
public class FabricaChevrolet implements FabricaCarro{

    @Override
    public Carro criarHatch(String versao, BigDecimal valor) {
     return new Onix(versao, valor);
    }
     public Carro criarSedan(String versao, BigDecimal valor) {
     return new Brisma(versao, valor);
    
    
}
}