/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod.fabrica;

import factorymethod.carro.Argo;
import factorymethod.carro.Carro;
import factorymethod.carro.Fiesta;
import factorymethod.carro.Gol;
import factorymethod.carro.Onix;
import factorymethod.constante.ModelosDeCarro;
import factorymethod.constante.Veiculos;
import factorymethod.interfaces.FabricaCarro;
import java.math.BigDecimal;
import javax.swing.JOptionPane;

public class CarroFactory {

    public Carro criarCarro(int categoria) {
        FabricaCarro fabrica;

        // Carro carro = null;   
        if (categoria == 1) {
            int modelo = Integer.valueOf(JOptionPane.showInputDialog(
                    "Escolha o seu Carro :\n 1- Argo 2- Fiesta  3- Gol  4-Onix :"));
            try {
                switch (Veiculos.getVeiculo(modelo)) {
                    case ARGO:
                        fabrica = new FabricaFiat();
                        // carro =
                        return fabrica.criarHatch("Drive 2018", new BigDecimal("30000"));
                    // break;                        
                    case FIESTA:
                        fabrica = new FabricaFord();
                        //carro =
                        return fabrica.criarHatch(" SE 1.6", new BigDecimal("45000"));
                    // break;                       
                    case GOL:
                        fabrica = new FabricaWolkswagen();
                        // carro =
                        return fabrica.criarHatch("Trindline", new BigDecimal("4000"));
                    // break;                        
                    case ONIX:
                        fabrica = new FabricaChevrolet();
                        //carro =
                        return fabrica.criarHatch("Advantage", new BigDecimal("35000"));
                    // break;
                    default:
                        JOptionPane.showMessageDialog(null, "Opçaõ invalida");
                }
            } catch (NullPointerException e) {
                JOptionPane.showMessageDialog(null, "Opçaõ invalida");
            }
        } else if (categoria == 2) {
            int modelo = Integer.valueOf(JOptionPane.showInputDialog(
                    "Escolha o seu Carro :\n 5- Brisma 6- Virtus  7- Cronos  8-Fiesta sedan:"));
            try {
                switch (Veiculos.getVeiculo(modelo)) {
                    case PRISMA:
                        fabrica = new FabricaChevrolet();
                        // carro =
                        return fabrica.criarSedan("LTZ", new BigDecimal("60000"));
                    // break;                        
                    case VIRTUS:
                        fabrica = new FabricaWolkswagen();
                        //carro =
                        return fabrica.criarHatch("Highline", new BigDecimal("80000"));
                    // break;                       
                    case FIESTA:
                        fabrica = new FabricaFord();
                        // carro =
                        return fabrica.criarHatch("SEL 1.6", new BigDecimal("9000"));
                    // break;                        
                    case CRONOS:
                        fabrica = new FabricaFiat();
                        //carro =
                        return fabrica.criarHatch("Precision ", new BigDecimal("85000"));
                    // break;
                    default:
                        JOptionPane.showMessageDialog(null, "Opçaõ invalida");
                }
            } catch (NullPointerException e) {
                JOptionPane.showMessageDialog(null, "Opçaõ invalida");
            }
            }else {
                  JOptionPane.showMessageDialog(null, "Opçaõ invalida");
                }
            return null;

        }
}
        
        /* public Carro criarCarro(int opcao){
        Carro carro = null;
       
      //  int opcao = Integer.valueOf( JOptionPane.showInputDialog("Escolha o seu Carro :\n 1- Argo 2- Fiesta  3- Gol  4-Onix :"));      
        switch(opcao){    
                    case ModelosDeCarro.ARGO:
                               carro = new Argo("Drive 2018", new BigDecimal("30000"));
                              
                        break;
                        
                    case ModelosDeCarro.fIESTA:
                        carro = new Fiesta(" SE 1.6", new BigDecimal("45000"));
                      
                        break;
                        
                    case ModelosDeCarro.GOL:
                        carro = new Gol("Trindline", new BigDecimal("4000"));
                        
                        break;
                        
                    case ModelosDeCarro.ONIX:
                        carro = new Onix("Advantage", new BigDecimal("35000"));
                        
                          break;
                          
                          default:
                              JOptionPane.showMessageDialog(null, "Opçaõ invalida");
        
                }
        return carro;
     }*/

        // USANDO AS CONTASTES EM ENUM 

