
package factorymethod.fabrica;

import factorymethod.carro.Carro;
import factorymethod.carro.Gol;
import factorymethod.carro.Onix;
import factorymethod.carro.Virtus;
import factorymethod.interfaces.FabricaCarro;
import java.math.BigDecimal;

/**
 *
 * @author Carleane sousa
 */
public class FabricaWolkswagen implements FabricaCarro{

    @Override
    public Carro criarHatch(String versao, BigDecimal valor) {
     return new Gol(versao, valor);
    }
     public Carro criarSedan(String versao, BigDecimal valor) {
     return new Virtus(versao, valor);
    
    
}
    
    
}
