
package factorymethod.fabrica;

import factorymethod.carro.Argo;
import factorymethod.carro.Carro;
import factorymethod.carro.Cronos;
import factorymethod.carro.Onix;
import factorymethod.interfaces.FabricaCarro;
import java.math.BigDecimal;

/**
 *
 * @author Carleane sousa
 */
public class FabricaFiat implements FabricaCarro{

    @Override
    public Carro criarHatch(String versao, BigDecimal valor) {
     return new Argo(versao, valor);
    }
     public Carro criarSedan(String versao, BigDecimal valor) {
     return new Cronos(versao, valor);
    
    
}
    
}
