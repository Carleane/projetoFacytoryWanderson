
package factorymethod.fabrica;

import factorymethod.carro.Carro;
import factorymethod.carro.Fiesta;
import factorymethod.carro.FiestaSedan;
import factorymethod.carro.Onix;
import factorymethod.interfaces.FabricaCarro;
import java.math.BigDecimal;

/**
 *
 * @author Carleane sousa
 */
public class FabricaFord implements FabricaCarro{

    @Override
     public Carro criarHatch(String versao, BigDecimal valor) {
     return new Fiesta(versao, valor);
    }
     public Carro criarSedan(String versao, BigDecimal valor) {
     return new FiestaSedan(versao, valor);
    
    
}
    
    
}
