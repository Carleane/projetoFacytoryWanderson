
package factorymethod.constante;

/**
 *
 * @author Carleane sousa
 */
public enum Veiculos {
       
        ARGO(1), FIESTA(2), GOL(3), ONIX(4), PRISMA(5), VIRTUS(6), CRONOS(7), FIESTASEDAN(8);
       
        private int valor;
    
          private Veiculos(int valor ){
          this.valor = valor;    
}
          public int getValor(){
              return this.valor;
          }
          public static Veiculos getVeiculo(int valor){
              for (Veiculos v  : Veiculos.values()) {
                  if(v.getValor() == valor){
                      return v;
                  }
                  
              }
              return null;
          }
          
}
